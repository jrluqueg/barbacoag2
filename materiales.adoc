== Lista de materiales

// Materiales necesarios por categoría, ordenados alfabéticamente

=== Cocina y comida

* Carbón vegetal ecológico
* Cucharas
* Platos
* Tenedores
* Vasos


=== Mobiliario

* Mesas


=== Diversión

* Radiocassette
